# Conductance-based DCM for electrophysiological data

## Pre-requisites

You will need MATLAB installed to run this code.
The code was developed using MATLAB 2020a and tested with MATLAB 2017b.
Plots are generated differently depending on your MATLAB version.


## Setup

Navigate to the repository folder and type in the MATLAB Command Window:

```
>> setup
```
You will get a message if a folder for the figures is created and the setup ran
successfully.
## Basis of implementation

To showcase model inversion, we have used 2-channel cross-spectral LFP data from an anaesthetized rodent. 
This dataset is freely available and can be downloaded from the [SPM website](https://www.fil.ion.ucl.ac.uk/spm/data/dcm_csd/).

We use [SPM12](https://www.fil.ion.ucl.ac.uk/spm/software/spm12/) to create our DCM models.
For details on the priors used, please refer to the following functions:
- [spm_cmm_NMDA_priors.m](external/spm12/toolbox/dcm_meeg/spm_cmm_NMDA_priors.m)
- [spm_ssr_priors.m](external/spm12/toolbox/dcm_meeg/spm_ssr_priors.m)
- [spm_L_priors.m](external/spm12/toolbox/dcm_meeg/spm_L_priors.m)

## Model inversion & Simulations
To run the code and generate the figures, you need to run the `generate_spectralplots.m` script.

```
>> generate_spectralplots
```
Alternatively, you can work your way through the code and plots step by step.
To do this, open the `generate_spectralplots.m` script, and run each block of code separately.

The default settings within `generate_spectralplots.m` are:

```
obsFlag         = 'CSD';
neuroFlag       = 'CMM_NMDA';
```

These will allow you to create a simple conductance-based DCM with the 4 neuronal
populations, and the AMPA, NMDA and GABA<sub>A</sub> receptors.

For **model inversion**, a DCM with 2 sources is created.

For the **simulations**, a first DCM with 1 source is defined.
This model is created to study the effect of within-source parameters on the
model output.
Further down the script, the number of sources is changed to `2`. In this second
model, we define 2 sources, with one forward connection from source 1 to source 2.
This model is created to study the effect of changing the between-source
connectivity parameters.

![models](simulations-models.png)

Both simulations will generate cross spectral densities.

The simulated data are plotted in linear scale by default. However, you can easily plot
data in log scale by changing the value of `logPlots` to `true`.

Finally, note that, in the publication, the results for a subset of the parameters
tested here are reported. Running the code gives access to a more comprehensive
assessment.

## Other plots
To reproduce the plot illustrating the magnesium block non-linearity, simply run
the following function:
```
>> mg_block_plot
```

## Saving figures
If you wish to save figures, please open the relevant `spectralplots*.m`
or `mg_block_plot.m` functions and change the value of `saveFigure` to `1`.

## Licence
This project is licensed under the GNU General Public License v3.0.
See `LICENCE.text` for details.

## Acknowledgements
Repository figure made with [Biorender.com](https://biorender.com/).


💻 Suggestions and feedback are always welcome!
