function label = var_to_label(DCM, param, index, connectionType)
% -------------------------------------------------------------------------
% var_to_label - Returns a label for each parameter.
%
%--------------------------------------------------------------------------
% INPUT:
%       DCM              - SPM DCM struct.
%
%       param            - char: variable name (e.g. 'T')
%
%       connectionType   - double: empty unless param passed is 'A' or
%                          'AN' (extrinsic connectivity parameters). Then
%                          you need to indicate whether you would like to
%                          alter a forward or backward connection. 
%                          Forward: 1, Backward: 2.
% 
% Optional:
%       index         - double: numel of in the DCM parameter class.

% 
%--------------------------------------------------------------------------
% OUTPUT: 
%       label    - char: Description of variable for plot. 
%       
%
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - April 2019
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------

% sources = {'lIPL', 'rIPL','lPFC','rPFC'};

    % Check input
    if ~ischar(param)
        error('Input must be a string.')
    end
    
    if ~exist('index','var')
        index=1;
    end
    
    % Get source names
    sources  = DCM.Sname;
    nSources = length(DCM.Sname);
    
    % Get constituents of neuronal model
    neuroFlag = DCM.options.model;
    switch neuroFlag
        case{'CMM_NMDA'}
            channels    = {'AMPA','GABA','NMDA'};
            populations = { 'spiny stellate cells' ...
                            'superficial pyramidal cells' ...
                            'inhibitory interneurons' ...
                            'deep pyramidal cells'
                          };
        % Complete with other cases
        % ...
    end
    
    nPopulations = length(populations);
    
    switch param
        case{'Mg'}
            label = '\alpha_{NMDA}, the magnesium switch parameter';
            
        case{'S'}
            label = '\Sigma^{(j)}';
   
        case{'T'}
            if nSources == 1
                source = 1;
                channel = index;
            elseif mod(index, nSources) == 0
                source = nSources;
                channel = fix(index/nSources);
            else
                source = mod(index, nSources);
                channel = fix(index/nSources) + 1;
            end
            label   = ['\kappa_{',channels{channel}, '} from ', sources{source}];
        
        case{'CV'}
            label = 'membrane capacitance';
            
        case{'E'}
            label = 'background noise'; 
            
        case{'H'}
            if index > nPopulations*nPopulations
                error('Right now, this code only allows changes in the first source.')
                % Expand to multiple sources if necessary.
            else
                source = sources{1};
            end

            to   = mod(index, nPopulations) ;
            if to == 0 
                to   = nPopulations;
                from = fix(index/nPopulations);
            else
                from = fix(index/nPopulations) + 1;
            end

            label = sprintf('the intrinsic connection from the\n %s to the %s in %s', populations{from}, populations{to}, source);
        
        case{'G'}
            label = 'intrinsic connectivity strengths';
            
        case{'A'}
            if connectionType == 1
                connectionType = 'forward';
            elseif connectionType == 2
                connectionType = 'backward';
            end

            to   = mod(index, nSources) ;
            if to == 0 
                to   = nSources;
                from = fix(index/nSources);
            else
                from = fix(index/nSources) + 1;
            end

            label = sprintf('the AMPA-mediated extrinsic\n %s connection from %s to %s', connectionType, sources{from}, sources{to});
    
        case{'AN'}
            if connectionType == 1
                connectionType = 'forward';
            elseif connectionType == 2
                connectionType = 'backward';
            end

            to   = mod(index, nSources) ;
            if to == 0 
                to   = nSources;
                from = fix(index/nSources);
            else
                from = fix(index/nSources) + 1;
            end

            label = sprintf('the NMDA-mediated extrinsic\n %s connection from %s to %s', connectionType, sources{from}, sources{to});
    
        case{'D'}
            label='delay parameter';   
            
        case{'L'}
            label='L_{\theta}';
    
        case{'J'}
            label='contributing state parameter';  
            
        case{'a'}
            if mod(index,2) % if we are in the first row of a --> amplitude
                label='a_1';
            elseif ~mod(index,2) % if we are in the 2nd row of a --> exponent
                label='a_2';
            end
            
        case{'b'}
            if index==1
                label='b_1';
            elseif index==2
                label='b_2';
            end
        
        case{'c'}
            if index==1
                label='c_1';
            elseif index==2
                label='c_2';
            end
            
        case{'d'}
            [nCoef, ns] = size(DCM.M.pE.d);
            if nSources ~= ns
                error('The second dimension of d is not equal to your number of sources.')
            end

            x = mod(index,nCoef);
            if x == 0
               x = nCoef;
               if nSources == 1
                   y = 1;
               else
                   y = fix(index/nSources);
               end
            elseif nSources > 1
               y = fix(index/nSources) + 1;
            elseif nSources == 1
                y = 1;
            end

            label=['d_{',num2str(x), num2str(y),'}, the ', num2str(index),'-th cosine function coefficient for ', sources{y}];
        
        case{'f'}
            label='filtering parameter';
    end
end