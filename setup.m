% Setup for tutorial paper codebase
%
% This script takes care of the necessary setup to run the main script (see
% generate_spectralplots.m) that reproduces the simulations from:
%
% Pereira et al. (2021)
%                      Conductance-Based Dynamic Causal Modeling 
%      - A Mathematical Review of its Application to Cross-Power Spectral Densities -
% 
% -------------------------------------------------------------------------
%
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - July 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
%-------------------------------------------------------------------------

rootdir = fileparts(which('setup'));

% Create directory to store figures
if ~exist(fullfile(rootdir, 'figs'), 'dir')
   mkdir(fullfile(rootdir, 'figs'))
   disp('Folder to store figures created.')
end

% Add repository paths
addpath(rootdir)
addpath((fullfile(rootdir, 'external')))
addpath(fullfile(rootdir, 'external','linspecer'))
addpath(fullfile(rootdir, 'external','spm12'))
addpath(fullfile(rootdir, 'external','spm12','toolbox','dcm_meeg'))
addpath(fullfile(rootdir, 'external','spm12','toolbox','spectral'))
addpath(fullfile(rootdir, 'external','spm12','matlabbatch'))
addpath(fullfile(rootdir, 'external','spm12','external','fieldtrip','utilities'))
addpath(fullfile(rootdir, 'external','dLFP_white_noise_r24_anaes'))

% To set white background in saved figures
set(0,'DefaultFigureColor',[1 1 1])

disp('Setup ran successfully.')