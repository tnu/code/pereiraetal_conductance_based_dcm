function spectralplots_param_neuronal(DCM, param, index, connectionType)
%% ------------------------------------------------------------------------
% spectralplots_param_neuronal - Function to reproduce Figure 13 of Pereira
%                                et al. (2021), Conductance-Based Dynamic
%                                Causal Modeling - A Mathematical Review of
%                                its Application to Cross-Power Spectral
%                                Densities.
%
%--------------------------------------------------------------------------
% INPUT:
%       DCM     - struct: an SPM DCM struct which contains all the necessary
%                         parameters needed for the simulations.
%                         (see generate_dcm.m).
%
%       param   - char: name of model parameter class to change. Follows
%                 DCM struct nomenclature.
% 
% Optional:
%       index   - double: numel within parameter class. This is the precise 
%                 parameter whose value will be changed in the simulations.
%                 Default options are set using the set_index.m function.
%
%       connectionType   - double: optional unless param passed is 'A' or
%                          'AN' (extrinsic connectivity parameters). Then
%                          you need to indicate whether you would like to
%                          alter a forward or backward connection. 
%                          Forward: 1, Backward: 2.
%
%--------------------------------------------------------------------------
% OUTPUT: 
%       A plot of the simulated CSD is generated when running this function. 
%           
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - April 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------

    global repoPath
    repoPath = fileparts(mfilename('fullpath'));
    
    % Check input
    %----------------------------------------------------------------------
    if ~exist('connectionType','var')
        connectionType = [];
    end
    
    if (~isequal(connectionType,1) && ~isequal(connectionType,2)) && (isequal(param, 'A') || isequal(param, 'AN'))
        error('You have supplied extrinsic connection parameters. Please indicate which connection type you would like to change. Forward: 1, Backward: 2.')
    end
    
    if ~exist('index','var')
       index = set_index(param); % Sets most appropriate index
    end

    % Define other variables
    %----------------------------------------------------------------------
    Hz         = DCM.M.Hz;     % Frequency interval of interest
    nSigma     = 2;            % Number of standard deviations

    if isequal(param, 'A') || isequal(param, 'AN')
        nSim   = 21;           % Total number of simulations
    else
        nSim   = 11;
    end
    
    if ~isequal(DCM.options.spatial, 'LFP') 
        % With LFP, L prior mean values are set to 1
        % If not LFP, need to change lead field to another value besides 0
        DCM.M.pE.L = ones(size(DCM.M.pE.L))*0.1; % 0.1 is an arbitrary value
    end
    
    % Generate new values for chosen parameter
    [newValues, dist] = generate_new_values(nSim, nSigma, DCM, param, index, connectionType);

    % Simulate data with prior values and new values
    %----------------------------------------------------------------------
    [csd_orig, csd_tot, nc] = simulate(DCM, param, newValues);
    if ~isequal(nc, 1)
        error('This function was only designed for single-source LFP models.')
    end
    
    % Plotting
    %----------------------------------------------------------------------
    
    % What to plot
%     func = {'abs','angle'};
%     lab = {'Magnitude', 'Phase'};
    func = {'abs'};
    lab = {'Magnitude'}; % we only plot the magnitude
    
    % Variables for plot aspect
    colors = linspecer(nSim);
    back_col = 0.15*[1 1 1];
    fontsize = 13;
    

    f = str2func(func{1});

    for ii=1:nSim
        if ~isequal(param,'Mg') && (ii == ceil(nSim/2) && mod(nSim,2)==1)
            plot(Hz,f(csd_orig(:,nc,nc)),'LineStyle','--','Color','w','LineWidth',2.25,'DisplayName', 'prior mean'); %,'-','Color',col,'linewidth',2);
            continue
        end
        % Fetch the simulated data
        csd = csd_tot{ii};
        % Plot it
        if ~isequal(param, 'Mg')
            displayNumber = num2str(dist(ii));
            if dist(ii) > 0
                displayNumber = ['+',displayNumber];
            end
            displayName = [displayNumber,'\bf{\sigma}']; 
        else
            displayName = ['\alpha_{NMDA} = ', num2str(newValues{ii}, '%.2f')]; 
        end
        if isequal(param, 'Mg') && ii == ceil(nSim/2) && mod(nSim,2)==1
            plot(Hz,f(csd(:,nc,nc)),'LineStyle','--','Color','w','LineWidth',2.25,'DisplayName', displayName);
        else
            plot(Hz,f(csd(:,nc,nc)),'Color',colors(ii,:),'LineWidth',1.25,'DisplayName', displayName);
        end
        hold on
    end

    % Regenerating plot with the prior value on top
    if ~isequal(param, 'd') && ~isequal(param, 'Mg')
        plot(Hz,f(csd_orig(:,nc,nc)),'LineStyle','--','Color','w','LineWidth',2.25,'Handlevisibility', 'off'); %,'-','Color',col,'linewidth',2);
    end
    xlim([min(Hz), max(Hz)])
        
    xlabel('Frequency (Hz)','FontSize', fontsize)
    ylabel(lab(1),'FontSize', fontsize)
    set(gca,'color',back_col)
    hold off
    
    % Title first
    switch param
        case{'T'}
            label = var_to_label(DCM, param, index);
            label = strtok(label);
        case{'H'}
            label = 'Intrinsic connectivity';
        case{'S'}
            label = var_to_label(DCM, param, index);
        case{'Mg'}
            label = '\alpha_{NMDA}';
    end
    title(label,'FontSize', fontsize)
    
    % Legend second
    lgd            = legend(gca,'show');
    lgd.TextColor  = 'white';
    lgd.Location   = 'Northeast';
    title(lgd,'Legend')

end