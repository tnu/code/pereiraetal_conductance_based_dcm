% demo for creating an SPM M/EEG dataset from arbitrary data using 
% conversion of simple Fieldtrip raw data struct. 
% SPM8 internal format is quite complex but is transparent to the user
% as meeg object's methods take care of maintaining its consistency. 
% The most straightforward way to convert arbitrary data that is available
% as an ASCII file or *.mat file with some variables to SPM8 is to create
% a quite simple Fieldtrip raw data struct and then use SPM's
% spm_eeg_ft2spm to convert this struct to SPM8 file. Missing information
% can then be supplemented using meeg methods and SPM functions.
% Fieldtrip raw struct must contain the following fields:
% .fsample - sampling rate (Hz)
% .trial - cell array of matrices with identical dimensions channels x time
% .time - cell array of time vectors (in sec), the same length as the
%         second dimension of the data. For SPM8 they must be identical.
% .label - cell array of strings, list of channel labels. Same length as
%         the first dimension of the data.
%__________________________________________________________________________
% Copyright (C) 2008 Wellcome Trust Centre for Neuroimaging

% Karl Friston, Vladimir Litvak 
% $Id: spm_lfp_txt2mat.m 2255 2008-09-30 15:36:59Z vladimir $


% load data (in this case a .mat file)
%--------------------------------------------------------------------------
clear all

files = {'White noise_20min_1_4iso.asc',...
    'White noise_20min_1_8iso.asc',...
    'White noise_20min_2_4iso.asc',...
    'White noise_20min_2_8iso.asc',...
    'White noise_20min_awake.asc'};

for session = 1:5
    fid = fopen(files{session})
    Raw_d{session} = textscan(fid, '%f %c %f %c %f %c %f %f %c %f %f %c %f',240000);
end


for session = 1:5
    start(session) = ((session-1)*240000+1);
    epoch = [start(session): (start(session)+240000-1)];
     for chan = 1:2
       if chan ==1
        data(epoch,1) = (ceil(Raw_d{session}{8})+(Raw_d{session}{10}/10000)) ;
        elseif chan ==2
        data(epoch,2)= (ceil(Raw_d{session}{11})+(Raw_d{session}{13}/10000)) ;  
        end
    end
end

% define the output file name
%--------------------------------------------------------------------------
fname = 'LFP_white_noise_r24';

% define the sampling rate
%--------------------------------------------------------------------------
fsample = 1000;
bins = start
nbins = 240000;

%%% tot length 1200001
% Define the channels of interest - in this case only 3,4 and 5
%--------------------------------------------------------------------------
Ic = [1 2];

% Create the Fieldtrip raw struct
%--------------------------------------------------------------------------
ftdata = [];

for i = 1:length(bins)
   It            = [1:nbins] + bins(i)-1;
   ftdata.trial{i} = data(It, Ic)';
   ftdata.time{i} = [0:(nbins-1)]./fsample;
end

colheaders ={'A1','A2'};
ftdata.fsample = fsample;
ftdata.label = colheaders(Ic);
ftdata.label = ftdata.label(:);

% Convert the ftdata struct to SPM M\EEG dataset
%--------------------------------------------------------------------------
D = spm_eeg_ft2spm(ftdata, fname);

% Examples of providing additional information in a script
% [] comes instead of an index vector and means that the command
% applies to all channels/all trials.
%--------------------------------------------------------------------------
D = type(D, 'single');             % Sets the dataset type
D = chantype(D, [], 'LFP');        % Sets the channel type 
D = conditions(D, [], {'Iso14';'Iso18';'Iso24';'Iso28';'awake'});  % Sets the condition label

% save
%--------------------------------------------------------------------------
save(D);
