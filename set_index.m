function index = set_index(param)
% -------------------------------------------------------------------------
% set_index - Sets index of most appropriate parameter to change in the
%             simulations.
%
%--------------------------------------------------------------------------
% INPUT:
%       param     - char: parameter class to consider. We follow DCM struct
%                   nomenclature for parameter class naming.
%
%--------------------------------------------------------------------------
% OUTPUT: 
%       index    - double: index of parameter within parameter class. 
%           
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - April 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------

    index = 1; % in general

    % Take exponent parameter if presented with a, b or c
    if isequal(param,'a') || isequal(param,'b') || isequal(param,'c')
        index = 2;
    elseif isequal(param,'H')
        index = 2; 
        % Excitatory connection to superficial pyramidal cells, which
        % are, according to spm_L_priors.m the main generators of signal.
        % This connection comes from the excitatory spiny stellate cells.
    elseif isequal(param,'T')
        index = 1; % 1 = AMPA, 2 = GABA, 3 = NMDA
    elseif isequal(param,'J')
         % Take cell population whose influence on final signal is parameterized
        index=3; % or 4 (3 = inhibitory interneurons, 4 = deep pyramidal cells)
    elseif isequal(param,'d')
        index=4; % Choose the highest frequency (in frequency space) for better visualization of effects
    end

end