function DCM = generate_dcm(obsFlag, neuroFlag, nSources, freqBand)
% -------------------------------------------------------------------------
% generate_dcm - Creates minimal DCM struct ready to use for simulations
%                of LFP data. Extensions need to be made to allow for
%                simulations of other data modalities.
%
%--------------------------------------------------------------------------
% INPUT:
%       obsFlag     - char: observation model flag. The current implementation
%                     only accepts 'CSD'. However, you, the user, can expand
%                     the function to meet your needs :)
%
%       neuroFlag   - char: neuronal model flag. The current implementation 
%                     only accepts 'CMM_NMDA'. As before, feel free to expand
%                     the function.
% 
%       nSources    - integer: number of sources to consider for model. You 
%                     can choose between 1, 2 or 4 source-models:
%                     - 1 source: mPFC (medial prefrontal cortex)
%                     - 2 sources: mPFC and PCC (posterior cingulate cortex)
%                     - 4 sources: mPFC, PCC, LIPC and RIPC (right and left
%                     inferior parietal cortices)
% 
% Optional:
%       freqBand    - 1 x 2 double: Frequency band to consider for 
%                     simulations later on. By default, the function 
%                     considers the 1-48 Hz frequency band.
%
%--------------------------------------------------------------------------
% OUTPUT: 
%       DCM         - a DCM struct which contains all the necessary
%                     information for the simulations.
%           
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - April 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------
        
    % Parse input
    obsValidOptions = {'CSD'};
    if ~ismember(obsFlag, obsValidOptions)
        error(sprintf('Please choose one the allowed flags: {%s}\n', strjoin(obsValidOptions, ', ')))
    end

    neuroValidOptions = {'CMM_NMDA'};
    if ~ismember(neuroFlag, neuroValidOptions)
        error(sprintf('Please choose one the allowed flags: {%s}\n', strjoin(neuroValidOptions, ', ')))
    end
   
    if nSources ~= 1 && nSources ~= 2 && nSources ~= 4 
        error('Please choose either 1, 2 or 4 sources.')
    end
    
    if ~exist('freqBand','var')
        freqBand = [1 50];
    end

    % Build generative model
    switch obsFlag
        case{'CSD'}
            DCM = define_dcm(obsFlag, neuroFlag, nSources, freqBand);
            DCM = define_dcm_csd(DCM);
        % Add more cases if wished
        % ...
    end
end

function DCM = define_dcm(obsFlag, neuroFlag, nSources, freqBand)
    
% DCM.M.Nmax   = 200;
    
switch obsFlag
    case{'CSD'}
        % -- Specifying DCM struct ----------------------------------------
        % Data
        DCM.xY.y              = [];
        DCM.xY.xy             = [];
        DCM.xY.modality       = 'LFP';

        % Options
        DCM.options.trials    = 1;         % Resting state
        DCM.options.analysis  = obsFlag;
        DCM.options.model     = neuroFlag;
        DCM.options.spatial   = 'LFP';
        DCM.options.Fdcm      = freqBand;
        DCM.options.Nmodes    = nSources;  % Channel modes - we consider one electrode per source
        DCM.options.D         = 0;

        % Sources and connectivity matrices
        if nSources == 1
            % Sources and connectivity matrices
            DCM.Lpos  = [   0 ;            % source coordinates (PCC)
                           -52 ;
                           26];

            DCM.Sname ={'source 1'};       % source name
            Ns        = length(DCM.Sname); % number of sources
            DCM.A{1}  = zeros(Ns,Ns);      % No forward connections
            DCM.A{2}  = zeros(Ns,Ns);      % No backward connections
            DCM.A{3}  = zeros(Ns,Ns);      % No lateral connections
            DCM.C     = 1';
            DCM.B     = [];
        
        elseif nSources == 2
            DCM.Lpos  =  [ 0,    3;
                           -52,  54;
                           26,  -2 ];

            DCM.Sname = {'source 1'; 'source 2'};
            Ns        = length(DCM.Sname);
            DCM.A{1}  = [0 0 ;             % Forward connection from PCC to mPFC
                         1 0 ]; 
            DCM.A{2}  = zeros(Ns,Ns);      % No backward connections
            DCM.A{3}  = zeros(Ns,Ns);      % No lateral connections
            DCM.C     = 1';
            DCM.B     = [];
            
        elseif nSources == 4
            DCM.Lpos  =  [  3,     0,    -50,    48;
                           54,   -52,    -63,   -69;
                           -2,    26,     32,    35];

            DCM.Sname = {'mPFC', 'PCC', 'LIPC', 'RIPC' };
            Ns        = length(DCM.Sname);
            DCM.A{1}  = [   0 1 1 1 ;
                            0 0 0 0 ;
                            0 0 0 0 ;
                            0 0 0 0  ] ;
            DCM.A{2}  = zeros(Ns,Ns);      % No backward connections
            DCM.A{3}  = zeros(Ns,Ns);      % No lateral connections
            DCM.C     = 1';
            DCM.B     = [];
        end

        % Design
        DCM.xU.X = [];

        % Model
        Nc                    = nSources;   % number of channels
        DCM.M.dipfit.model    = neuroFlag;
        DCM.M.dipfit.type     = DCM.options.spatial;
        DCM.M.dipfit.Ns       = Ns;
        DCM.M.dipfit.Nc       = Nc;
        DCM.M.dipfit.silent_source  = strfind(DCM.Sname,'silent');
        DCM.M.dipfit.Ic       = 1:DCM.M.dipfit.Nc; % indices of good channels
        DCM.M.dipfit.location = 0;
        DCM.M.dipfit.symmetry = 0;
        DCM.M.dipfit.Lpos     = DCM.Lpos;
        DCM.M.dipfit.radius   = 16;
        DCM.M.dipfit.modality = DCM.xY.modality;
        DCM.M.Hz              = DCM.options.Fdcm(1):DCM.options.Fdcm(2);
        DCM.M.l               = DCM.options.Nmodes;
end
    
end