function spectralplots_param_change_band_comp(DCM, param, index)
%% ------------------------------------------------------------------------
% spectralplots_param_change_band_comp - Creates spectral plots by changing 
%                                       the value of one parameter and 
%                                       observing how the spectral plots 
%                                       are changed. Everything is plotted 
%                                       in the same plot for better 
%                                       visualization. Also compares 2 bands. 
%                                       Only works well for 1-source models.
%--------------------------------------------------------------------------
% INPUT:
%       DCM     - struct: an SPM DCM struct which contains all the necessary
%                         parameters needed for the simulations.
%                         (see generate_dcm.m).
%
%       param   - char: name of model parameter class to change. Follows
%                 DCM struct nomenclature.
% 
% Optional:
%       index   - double: numel within parameter class. This is the precise 
%                 parameter whose value will be changed in the simulations.
%                 Default options are set using the set_index.m function.
%       
%--------------------------------------------------------------------------
% OUTPUT: 
%       A plot of the simulated CSD is generated when running this function. 
%       This plot can be automatically saved in the figs/ folder by setting:
%       saveFigure = 1;
%           
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - April 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------

    global repoPath
    repoPath = fileparts(mfilename('fullpath'));
    
    saveFigure = 1;
    
    % Get Matlab version for plotting
    [~, d] = version;
    d = datetime(d,'InputFormat','MMMM d, yyyy');
    
    % Check input
    %----------------------------------------------------------------------
    if ~exist('index','var')
       index = set_index(param); % Sets most appropriate index
    end
    
    if isequal(param, 'A') || isequal(param, 'AN')
        error('This function was only designed for single-source LFP models.')
    end

    % Define other variables
    %----------------------------------------------------------------------
    Hz         = DCM.M.Hz;     % Primary frequency interval of interest
   
    % Takes delta-theta-alpha band for comparison
    if isequal(param,'d')
        secondBandBeg = 1;  
    elseif isequal(param,'b') || isequal(param,'c')
        secondBandBeg = 40;  
    else
        secondBandBeg = 20;
    end  
    
    if isequal(param,'d')
        secondBandEnd = 13;
    elseif isequal(param,'b') || isequal(param,'c')
        secondBandEnd = 50;
    else
        secondBandEnd = 50;
    end
    
    if isequal(param,'d')
        secondBand = 'Delta-theta-alpha'; 
    elseif isequal(param,'b') || isequal(param,'c')   
        secondBand = 'Gamma'; 
    else
        secondBand = 'Beta-gamma';  
    end
    bands         = {[Hz(1) Hz(end)], [secondBandBeg secondBandEnd]};
    bandNames     = {'Full', secondBand};
    commonIdx     = find(Hz >=secondBandBeg & Hz <= secondBandEnd);
    
    % Define number of simulations
    nSim   = 11;           % Total number of simulations
    
    % Number of standard deviations
    if isequal(param,'d'), nSigma = 5; else; nSigma = 2;  end         
    
    if ~isequal(DCM.options.spatial, 'LFP') 
        % With LFP, L values are set to 1
        % If not LFP, need to change lead field to another value besides 0
        DCM.M.pE.L = ones(size(DCM.M.pE.L))*0.1; % arbitrary value
    end
    
    % Generate new values for chosen parameter
    [newValues, dist] = generate_new_values(nSim, nSigma, DCM, param, index);
%     if isequal(param, 'Mg'); nSim = length(newValues); end % correct number for Mg

    % Simulate data with prior values and new values
    %----------------------------------------------------------------------
    [csd_orig, csd_tot, nc] = simulate(DCM, param, newValues);
    if ~isequal(nc, 1)
        error('This function was only designed for single-source LFP models.')
    end

    
    % Plotting
    %----------------------------------------------------------------------
    
    % What to plot
%     func = {'abs','angle'};
%     lab = {'Magnitude', 'Phase'};
    func = {'abs'};
    lab = {'Magnitude'}; % we only plot the magnitude
    
    % Variables for plot aspect
    colors   = linspecer(nSim);
    back_col = 0.15*[1 1 1];
    fontsize = 13;
  
    figure()
%     set(gcf, 'Position',  [0, 0, 800, 550])
    for ii=1:length(bands)
        subplot(1, length(bands), ii)
            f = str2func(func{1});

                for jj=1:nSim
                    if (jj == ceil(nSim/2) && mod(nSim,2)==1) && ~isequal(param,'d')
                        if ~isequal(param,'Mg')
                            displayName = 'prior mean';
                        else
                            displayName = ['\alpha_{NMDA} = ', num2str(newValues{jj}, '%.2f')];
                        end
                        plot(Hz,f(csd_orig(:,nc,nc)),'LineStyle','--','Color','w','LineWidth',2.25,'DisplayName', displayName); %,'-','Color',col,'linewidth',2);
                        continue % this would correspond to prior value which we already plot later on.
                    end
                    % Fetch the simulated data
                    csd = csd_tot{jj};
                    if ~isequal(param, 'Mg')
                        displayNumber = num2str(dist(jj));
                        if dist(jj) > 0
                            displayNumber = ['+',displayNumber];
                        end
                        displayName = [displayNumber,'\bf{\sigma}'];
                    else
                        displayName = ['\alpha_{NMDA} = ', num2str(newValues{jj}, '%.2f')];
                    end
                    
                    if ii == 1
                        plot(Hz,f(csd(:,nc,nc)),'Color',colors(jj,:),'LineWidth',1.25,'DisplayName', displayName); %,'-','Color',col,'linewidth',2);
                    elseif ii == 2
                        plot(Hz(commonIdx), f(csd(commonIdx,nc,nc)), 'Color',colors(jj,:),'LineWidth',1.25,'DisplayName', displayName);
                    end
                    hold on
                end

                % Generating plot with the prior value on top
                if ~isequal(param, 'd') && ~isequal(param, 'Mg')
                    if ii == 1
                        plot(Hz,f(csd_orig(:,nc,nc)),'LineStyle','--','Color','w','LineWidth',2.25,'Handlevisibility', 'off'); %,'-','Color',col,'linewidth',2);
                        xlim([min(Hz), max(Hz)])
                    elseif ii == 2
                        plot(Hz(commonIdx),f(csd_orig(commonIdx,nc,nc)),'LineStyle','--','Color','w','LineWidth',2.25,'Handlevisibility', 'off'); %,'-','Color',col,'linewidth',2);
                        xlim([secondBandBeg, secondBandEnd])
                    end
                else 
                    if ii == 1
                        xlim([min(Hz), max(Hz)])
                    elseif ii == 2   
                        xlim([secondBandBeg, secondBandEnd])
                    end
                end
                title([bandNames{ii}, ' band'],'FontSize', fontsize)
                xlabel('Frequency (Hz)','FontSize', fontsize)
                ylabel(lab(1),'FontSize', fontsize)
                set(gca,'color',back_col)
    end 
    hold off
    
    % Title first
    label = var_to_label(DCM, param, index);
    titleStr = ['Changing the value of ',label];
    if d > datetime(2018,01,01)
        sgtitle(titleStr, 'FontSize', fontsize+2)
    else
        set(gcf,'name',titleStr)
    end
    
    % Legend second
    lgd            = legend(gca,'show');
    lgd.TextColor  = 'white';
    lgd.Location   = 'Northeast';
    title(lgd,'Legend')
    
    % Save figure
    if saveFigure
        set(gcf, 'InvertHardCopy', 'off'); % to keep the background as in figure
        if isequal(param, 'd')
            label = 'cosine-functions-band-comp';
        else
            label = [param, '-band-comp'];
        end
        outFile = fullfile(repoPath, 'figs', [label, '.png']);
        saveas(gcf, outFile)
    end
end