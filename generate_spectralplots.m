%% ------------------------------------------------------------------------
% Simulation script - conductance-based DCM for cross-spectral data
%
% This script performs the simulations and generates the plots presented in
% the publication: 
%
% Pereira et al. (2021)
%                      Conductance-Based Dynamic Causal Modeling 
%      - A Mathematical Review of its Application to Cross-Power Spectral Densities -
%
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - July 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------

%% Initial setup

% Cleaning environment
clear; clc
close all

%% Setting main model and visualziation variables

obsFlag    = 'CSD';
neuroFlag  = 'CMM_NMDA';
logPlots   = false;


%% Model inversion on empirical LFP data (data under external/dLFP_white_noise_r24_anaes)

% Generate DCM with 2 sources
nSources = 2;
DCM      = generate_dcm(obsFlag, neuroFlag, nSources);

% Invert model
frange   = [1 50]; % Define frequency range of interest (arbitrary)
fit_dcm_empirical_lfp(DCM,frange);

%% Simulations - Model 1: Generate DCM to get priors 

nSources   = 1;
DCM        = generate_dcm(obsFlag, neuroFlag, nSources);

%% Preprocess parameter list

pE = DCM.M.pE;
pC = DCM.M.pC;
params = fieldnames(pE);

% Remove empty fields from parameter list
idx = cellfun(@(x) isempty(pE.(x)), params);
params(idx) = [];

% Detect parameters which are cells (these have to be handled separately)
idx = cellfun(@(x) iscell(pE.(x)), params);
params_cell = params(idx);
params(idx) = [];

% Remove fixed parameters
idx = cellfun(@(x) isequal(pC.(x),zeros(size(pC.(x)))), params);
params(idx) = [];

% Remove lead field matrix parameters
params(ismember(params,'L')) = [];

%% Make plots for Model 1
for p=1:length(params)
    param=params{p}; % Extract the string from the cell
    if logPlots
        spectralplots_param_change_log(DCM, param) 
        % Precise parameter index used depends on variable considered. See
        % function for details.
        spectralplots_param_change_band_comp_log(DCM, param)
    else
        spectralplots_param_change(DCM, param) 
        % Precise parameter index used depends on variable considered. See
        % function for details.
        spectralplots_param_change_band_comp(DCM, param)
    end
end

%% Reproducing first Figure of simulation part of publication
paramNeuronal = {'T', 'H', 'S', 'Mg'};
subplotLabels = {'A', 'B', 'C', 'D'};
figure()
set(gcf, 'Position',  [0, 0, 900, 650])
for ii=1:length(paramNeuronal)
    subplot(2, 2, ii)
    if logPlots
        spectralplots_param_neuronal_log(DCM, paramNeuronal{ii})
    else
        spectralplots_param_neuronal(DCM, paramNeuronal{ii})
    end
    text(0.05,0.98,subplotLabels{ii},'Units', 'Normalized', 'VerticalAlignment', 'Top', 'Color', 'white', 'FontSize', 16)
end
set(gcf, 'InvertHardCopy', 'off'); % to keep the background as in figure
outFile = fullfile('figs', ['simulations-fig.png']);
saveas(gcf, outFile)

%% Define Model 2 and make plots

% Generate new DCM with 2 sources
nSources       = 2;
DCM            = generate_dcm(obsFlag, neuroFlag, nSources);
index          = 2; % we want to change forward connection from source 1 to source 2
connectionType = 1; % 1 for forward, 2 for backward

% Loop over the extrinsic connectivity parameters
for p=1:length(params_cell)
    param_cell=params_cell{p};
    if logPlots
        spectralplots_param_change_log(DCM, param_cell, index, connectionType)
    else
        spectralplots_param_change(DCM, param_cell, index, connectionType)
    end
end



