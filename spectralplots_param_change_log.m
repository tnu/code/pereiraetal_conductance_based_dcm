function spectralplots_param_change_log(DCM, param, index, connectionType)
% -------------------------------------------------------------------------
% spectralplots_param_change - Creates spectral plots by changing the value 
%                              of one parameter and observing how the 
%                              spectral plots are changed. Everything is 
%                              plotted in the same plot for better 
%                              visualization.
%
%--------------------------------------------------------------------------
% INPUT:
%       DCM     - struct: an SPM DCM struct which contains all the necessary
%                         parameters needed for the simulations.
%                         (see generate_dcm.m).
%
%       param   - char: name of model parameter class to change. Follows
%                 DCM struct nomenclature.
% 
% Optional:
%       index   - double: numel within parameter class. This is the precise 
%                 parameter whose value will be changed in the simulations.
%                 Default options are set using the set_index.m function.
%
%       connectionType   - double: optional unless param passed is 'A' or
%                          'AN' (extrinsic connectivity parameters). Then
%                          you need to indicate whether you would like to
%                          alter a forward or backward connection. 
%                          Forward: 1, Backward: 2.
%--------------------------------------------------------------------------
% OUTPUT: 
%       A plot of the simulated CSD is generated when running this function. 
%       This plot can be automatically saved in the figs/ folder by setting:
%       saveFigure = 1;
%           
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - April 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------

    global repoPath
    repoPath = fileparts(mfilename('fullpath'));
    
    saveFigure = 1;
    
    % Get Matlab version for plotting
    [~, d] = version;
    d = datetime(d,'InputFormat','MMMM d, yyyy');
    
    % Check input
    %----------------------------------------------------------------------
    if ~exist('connectionType','var')
        connectionType = [];
    end
    
    if (~isequal(connectionType,1) && ~isequal(connectionType,2)) && (isequal(param, 'A') || isequal(param, 'AN'))
        error('You have supplied extrinsic connection parameters. Please indicate which connection type you would like to change. Forward: 1, Backward: 2.')
    end
    
    if ~exist('index','var')
       index = set_index(param); % Sets most appropriate index
    end

    % Define other variables
    %----------------------------------------------------------------------
    Hz         = DCM.M.Hz;     % Frequency interval of interest
    nSigma     = 2;            % Number of standard deviations
    if isequal(param, 'A') || isequal(param, 'AN')
        nSim   = 21;           % Total number of simulations
    else
        nSim   = 11;
    end
    
    if ~isequal(DCM.options.spatial, 'LFP') 
        % With LFP, L prior mean values are set to 1
        % If not LFP, need to change lead field to another value besides 0
        DCM.M.pE.L = ones(size(DCM.M.pE.L))*0.1; % 0.1 is an arbitrary value
    end
    
    % Generate new values for chosen parameter
    [newValues, dist] = generate_new_values(nSim, nSigma, DCM, param, index, connectionType);

    % Simulate data with prior values and new values
    %----------------------------------------------------------------------
    [csd_orig, csd_tot, nc] = simulate(DCM, param, newValues);
    
    % Plotting
    %----------------------------------------------------------------------
    
    % What to plot
%     func = {'abs','angle'};
%     lab = {'Magnitude', 'Phase'};
    func = {'abs'};
    lab = {'Magnitude'}; % we only plot the magnitude
    
    % Variables for plot aspect
    colors = linspecer(nSim);
    back_col = 0.15*[1 1 1];
    fontsize = 13;
    
    for ii=1:length(func)
        f = str2func(func{ii});
        if nc > 4
%             figure('units','normalized','outerposition',[0 0 1 1])
        else 
            figure()
%             set(gcf, 'Position',  [0, 0, 800, 550])
        end
        for jj=1:nc
            for kk=1:nc
                if jj>kk
                    continue
                else
                    subplot(nc,nc,nc*(jj-1)+kk)
                    for ll=1:nSim
                        if ll == ceil(nSim/2) && mod(nSim,2)==1
                            if ~isequal(param,'Mg')
                                displaynameStr = 'prior mean';
                            else
                                displaynameStr = ['\alpha_{NMDA} = ', num2str(newValues{ll}, '%.2f')];
                            end
                                plot(Hz,log10(f(csd_orig(:,jj,kk))),'LineStyle','--','Color','w','LineWidth',2.25,'DisplayName', displaynameStr); %,'-','Color',col,'linewidth',2);
                                continue % this would correspond to prior value which we already plot later on.
                        end
                        if ~isequal(param, 'Mg')
                            displayNumber = num2str(dist(ll));
                            if dist(ll) > 0
                                displayNumber = ['+',displayNumber];
                            end
                            displaynameStr = [displayNumber,'\bf{\sigma}'];
                        else
                            displaynameStr = ['\alpha_{NMDA} = ', num2str(newValues{ll}, '%.2f')];
                        end
                        csd = csd_tot{ll}; % Fetch the simulated data
                        if d > datetime(2018,01,01) || (d < datetime(2018,01,01) && DCM.options.Nmodes < 2) || (d < datetime(2018,01,01) && DCM.options.Nmodes == 2 && mod(ll,2))
                            plot(Hz,log10(f(csd(:,jj,kk))),'Color',colors(ll,:),'LineWidth',1.25,'DisplayName', displaynameStr); %,'-','Color',col,'linewidth',2);
                        elseif (d < datetime(2018,01,01) && DCM.options.Nmodes == 2 && ~mod(ll,2)) || mod(ll,2)
                            plot(Hz,log10(f(csd(:,jj,kk))),'Color',colors(ll,:),'LineWidth',1.25,'DisplayName', displaynameStr, 'HandleVisibility','off'); %,'-','Color',col,'linewidth',2);
                        end
                        hold on
                    end
                    
                    % Regenerating plot with the prior value on top
                    if ~isequal(param, 'Mg')
                        plot(Hz,log10(f(csd_orig(:,jj,kk))),'LineStyle','--','Color','w','LineWidth',2.25,'Handlevisibility', 'off'); %,'-','Color',col,'linewidth',2);
                    end
                    hold on;
                    title(['CSD ' num2str(jj) '/' num2str(kk)])
                    if jj == kk; xlabel('Frequency (Hz)','FontSize', fontsize); end
                    if kk == jj; ylabel(['Log', lab(ii)],'FontSize', fontsize); end
                    set(gca,'color',back_col)
                    xlim([min(Hz), max(Hz)])
                end
            end
        end
        
        % Title
        label = var_to_label(DCM, param, index, connectionType);
        titleStr = ['Changing the value of ',label];
        if d > datetime(2018,01,01)
            sgtitle(titleStr, 'FontSize', fontsize+2)
        else
            set(gcf,'name',titleStr)
        end

        % Legend
        lgd = legend(gca,'show');
        lgd.TextColor = 'white';
        title(lgd,'Legend')

        if DCM.options.Nmodes == 2
            % Creating dummy subplot to position legend more elegantly
            hL = subplot(nc,nc,3);
            title('')
            poshL = get(hL,'position'); % Getting its position
            axis(hL,'off');             % Turning axis off so you don't see it
            if d > datetime(2018,01,01)
                lgd.NumColumns = 2;
            end
            lgd.Position   = poshL;
        else
            lgd.Location = 'Northeast';
        end
        hold off
    end
    
    % Save figure
    if saveFigure
        set(gcf, 'InvertHardCopy', 'off'); % to keep the background as in figure
        if isequal(param, 'A') || isequal(param, 'AN')
            label = [param, '-extr'];
        elseif isequal(param, 'd')
            label = 'cosine-functions';
        else
            label = param;
        end
        outFile = fullfile(repoPath, 'figs', ['log-', label, '.png']);
        saveas(gcf, outFile)
    end
end