function mg_block_plot()
% -------------------------------------------------------------------------
% mg_block_plot - Plots the magnesium (mg) block non-linearity observed for 
%                 the NMDA receptors.
%
%--------------------------------------------------------------------------
% INPUT:
%       no input
%
%--------------------------------------------------------------------------
% OUTPUT: 
%       A plot is generated when running this function. This plot can be
%       automatically saved in the figs/ folder by setting: saveFigure = 1;
%           
% -------------------------------------------------------------------------
% REFERENCES:
%
% 1. Pereira et al. (2021)
%                      Conductance-Based Dynamic Causal Modeling 
%      - A Mathematical Review of its Application to Cross-Power Spectral Densities -
%
% 2. Moran et al. (2011) Consistent spectral predictors for dynamic causal models of steady-state responses
%
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - July 2020
%           Based on Figure 1 from: 
%           Moritz Gruber, "Physiological Generative Modeling of EEG in Anti-NMDA Receptor Autoimmune Encephalitis", 
%           Master Thesis, UZH & ETHZ, 2019.
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------
    
    % Defining whether to save figure
    saveFigure = 0;

    % Defining Mg non-linearity variables as per Moran et al. 2011
    V            =   linspace(-80,50,1000); % voltage
    scale        =   1;                     % numerator of Equation 6
    slope        =   0.2;
    sens         =   0.06;                  % alpha_NMDA parameter
    sens_list    =   [0, 0.02, 0.04, 0.05, 0.07, 0.09, 0.11, 0.12, 0.14, 0.16];
    VN           =   10;                    % Reversal potential for NMDA
    n            =   length(sens_list);     % number of parameters tested
    m            =   zeros(1,length(V));

    % Plot aspect variables
    colors       =   linspecer(n);
    fontsize     =   14;
    y_lim        =   [-110, 40];

    % Create m(V)
    m_base       =   zeros(1,length(V));
    for ii=1:length(V)
        m_base(ii) = scale/(1+slope*exp(-sens*V(ii)));
    end

    % Create base plot 
    y = m_base.*(V-VN);
    str = ['\alpha_{NMDA} = ', num2str(sens)];
    plot(V,y,'k','LineWidth',2,'DisplayName',str)
    xlabel('Membrane voltage (mV)','FontSize',fontsize+2)
    ylabel('m(V) \cdot (V-V_{NMDA})','FontSize',fontsize+2)
    ax = gca;
    ax.FontSize = fontsize;
    hold on
    grid on

    for ii=1:n
        sens_ex = sens_list(ii);
        for jj=1:length(V)
            m(jj) = scale/(1+slope*exp(-sens_ex*V(jj)));
        end
        y = m.*(V-VN);
        plot(V,y,'Color',colors(ii,:),'LineWidth',1,'DisplayName',['\alpha_{NMDA} = ',num2str(sens_ex, '%.2f')])
        title('Changing \alpha_{NMDA}','FontSize', fontsize+2)
    end
    ylim(y_lim);
    xlim([min(V), max(V)])
    xLimits = get(gca,'XLim');  %# Get the range of the x axis
    yLimits = get(gca,'YLim');  %# Get the range of the y axis
    line('XData', [xLimits(1) xLimits(2)], 'YData', [0 0],'HandleVisibility','off')
    line('XData', [0 0], 'YData', [yLimits(1) yLimits(2)],'HandleVisibility','off')
    legend(gca,'show','Location','best')
    hold off
    
    % Save figure
    if saveFigure
        repoPath = fileparts(mfilename('fullpath'));
        outFile  = fullfile(repoPath, 'figs', 'mg-block.png');
        saveas(gcf, outFile)
    end

end