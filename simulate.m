function [csd_orig, csd_tot, nc] = simulate(DCM, param, newValues)
% -------------------------------------------------------------------------
% simulate - Performs all simulations. 
%
%--------------------------------------------------------------------------
% INPUT:
%       DCM         - struct: an SPM DCM struct which contains all the 
%                     necessary parameters needed for the simulations.
%
%       param       - char: parameter class whose parameters were changed.
%
%       newValues   - 1 x nSim cell: Contains the new parameter values for
%                     the whole parameter class.
%
%--------------------------------------------------------------------------
% OUTPUT: 
%       csd_orig    - Cross-spectral densities (CSD) simulated with prior
%                     values.
%
%       csd_tot     - 1 x nSim cell: CSD for all simulations.
%           
% -------------------------------------------------------------------------
% 
% pereira.inez@gmail.com
%
% Author:   Ines Pereira, TNU, UZH & ETHZ - June 2020
% Copyright 2020 by Ines Pereira <pereira.inez@gmail.com>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% -------------------------------------------------------------------------

    % Simulate data with prior values
    %----------------------------------------------------------------------
    DCM_orig = DCM;
    csd_orig = generate_csd(DCM_orig);
    csd_orig = csd_orig{:};
    [~, nc, ~] = size(csd_orig); 

    % Simulate data with new values
    %----------------------------------------------------------------------
    nSim = length(newValues);
    csd_tot = cell(1,nSim);
    for ii=1:nSim
        DCM = DCM_orig;
        % Generate the simulated data
        DCM.M.pE.(param) = newValues{ii};
        csd = generate_csd(DCM);
        csd_tot(ii) = csd;
    end

end